import React, {Component} from 'react';
import ReactDOM from 'react-dom'
import 'bootstrap/dist/css/bootstrap.css'
import Modal from "./components/Modal";

class App extends Component {

    state = {
        myUsers: [],
        modalVisible: false,
        searchValue: ''
    }

    removeUser = (index) => {
        let myUsersStr = localStorage.getItem('users');
        if (myUsersStr) {
            let myUsers = JSON.parse(myUsersStr)
            myUsers.splice(index, 1);

            this.setState({
                myUsers: myUsers
            })

            localStorage.setItem('users', JSON.stringify(myUsers))
        }
    }

    saveUser = (user) => {
        let {myUsers} = this.state

        let clone = [...myUsers]
        if (user.firstName !== '' || user.firstName !== '') {
            clone.push(user)
        } else {
            alert("You have to enter at least firstName and lastName")
        }

        this.setState({
            myUsers: clone
        })

        localStorage.setItem("users", JSON.stringify(clone))
        this.hideModal()
    }

    componentDidMount() {
        let usersStr = localStorage.getItem("users");
        if (usersStr) {
            let usersArray = JSON.parse(usersStr)
            this.setState({
                myUsers: usersArray
            })
        }
    }

    hideModal = () => {
        this.setState({
            modalVisible: false
        })

    }

    showModal = () => {
        this.setState({
            modalVisible: true
        })
    }

    handleChange = (event) => {
        console.log(event.target.name, event.target.value)
        this.setState({
            searchValue: event.target.value
        })
    }

    findUser = (searchValue) => {
        let {myUsers} = this.state

        let clone = myUsers.filter((item) => item.firstName === searchValue || item.lastName === searchValue)

        this.setState({
            myUsers: clone,
            searchValue: ''
        })
    }

    resetTable = () => {
        let usersStr = localStorage.getItem("users");
        if (usersStr) {
            let usersArray = JSON.parse(usersStr)
            this.setState({
                myUsers: usersArray,
                inputValue: ''
            })
        }

    }

    render() {

        const {myUsers, modalVisible, searchValue} = this.state

        return <div className={'container mt-5'}>
            <div className="row">
                <div className="col-md-12">
                    <div className="row">
                        <div className="col-md-4">
                            <form action="#" className={'form-control text-right'}>
                                <input
                                    type="text"
                                    className={'py-1 px-2 form-control'}
                                    placeholder={'Search'}
                                    name='search'
                                    value={searchValue}
                                    onChange={this.handleChange}
                                />
                                <button className={'btn btn-info d-inline-block mt-2'}
                                        onClick={() => this.findUser(searchValue)}>Search
                                </button>
                                <button className={'btn btn-warning d-inline-block mt-2 ml-2'}
                                        onClick={this.resetTable}>Reset
                                </button>
                            </form>

                        </div>
                        <div className="col-md-5">
                            {modalVisible ?
                                <Modal saveUser={this.saveUser}
                                       hideModal={this.hideModal}/> : ''}
                        </div>
                        <div className="col-md-3 text-right">
                            <button onClick={this.showModal} className={'btn btn-success'}>Add user</button>
                        </div>
                        <div className="col-md-12 mt-3">
                            <table className="table table-striped">
                                <thead>
                                <tr>
                                    <th>N</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Age</th>
                                    <th>Mail</th>
                                    <th></th>
                                </tr>
                                </thead>

                                <tbody>
                                {
                                    myUsers.map((item, index) => <tr key={index}>
                                            <td>{index + 1}</td>
                                            <td>{item.firstName}</td>
                                            <td>{item.lastName}</td>
                                            <td>{item.age}</td>
                                            <td>{item.mail}</td>
                                            <td>
                                                <button className={'btn btn-danger'}
                                                        onClick={() => this.removeUser(index)}>Remove
                                                </button>
                                            </td>
                                        </tr>
                                    )
                                }
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    }
}

export default App

ReactDOM.render(
    <App/>
    , document.getElementById('root')
)