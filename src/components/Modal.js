import React, {Component} from 'react';

class Modal extends Component {

    state = {
        user: {
            firstName: '',
            lastName: '',
            age: '',
            mail: ''
        }
    }

    onCancelClicked = () => {
        this.props.hideModal()
    }

    saveUser = () => {
        let {user} = this.state
        // console.log(user)
        this.props.saveUser(user)
    }

    handleChange = (event) => {
        console.log(event.target.name, event.target.value)
        this.setState(prevState => ({
            user: {
                ...prevState.user,
                [event.target.name]: event.target.value
            }
        }))
    }

    onSubmitClicked = (event) => {
        event.preventDefault();
    }

    render() {

        const {user} = this.state
        return (
            <div className={'card'}>
                <div className="card-header">
                    <h4>Add user</h4>
                </div>
                <div className="card-body">

                    <form onSubmit={this.onSubmitClicked} name={'myForm'} className={'form-control'}>
                        <div className="row">
                            <div className="col-md-12">
                                <h6>First Name</h6>
                                <input className={'py-1 px-2 mb-2 form-control'} type="text"
                                       onChange={this.handleChange}
                                       name="firstName"
                                       value={user.firstName}
                                       required="required"
                                       placeholder={'Enter your first name '}/>
                            </div>
                            <div className="col-md-12">
                                <h6>Last Name</h6>
                                <input className={'py-1 px-2 mb-2 form-control'} type="text"
                                       onChange={this.handleChange}
                                       name="lastName"
                                       required="required"
                                       value={user.lastName}
                                       placeholder={'Enter your last name '}/>
                            </div>
                            <hr className={'font-weight-bold'}/>
                            <div className="col-md-12">
                                <h6>Age</h6>
                                <input className={'py-1 px-2 mb-2 form-control'} type="number"
                                       onChange={this.handleChange}
                                       name="age"
                                       required="required"
                                       value={user.age}
                                       placeholder={'Enter your age'}/>
                            </div>
                            <div className="col-md-12">
                                <h6>Email</h6>
                                <input className={'py-1 px-2 mb-2 form-control'} type="email"
                                       onChange={this.handleChange}
                                       name="mail"
                                       required="required"
                                       value={user.mail}
                                       placeholder={'Enter your email'}/>
                            </div>
                        </div>
                    </form>

                </div>
                <div className="card-footer">
                    <button className="btn btn-success" onClick={this.saveUser} form={"myForm"}>Save</button>
                    <button className="btn btn-danger ml-2" onClick={this.onCancelClicked}>Cancel</button>
                </div>
            </div>
        );
    }
}

export default Modal;